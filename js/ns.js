function getTrainInfo(){
	//http://www.vid.nl/VI/overzicht
	var rssurl = 'https://www.rijdendetreinen.nl/rss/';
	$("#train").html('');
	$("#train").rss(rssurl,{
		layoutTemplate: "{entries}",
		entryTemplate: '<div class="col-md-12 no-padding trainrow" data-toggle="modal" data-target="#trainweb" onclick="setSrc(this);"><div class="transbg"><strong>{title}</strong><br/>{shortBodyPlain}</div></div>',
		success: function(){ 
			if($('.trainrow').length==0){ 
				$('.containstrain').hide(); 
			}
			else {
				$("#train").html('<div class="col-md-12 no-padding trainrow" data-toggle="modal" data-target="#trainweb" onclick="setSrc(this);"><div class="transbg"><strong>Er zijn '+$('.trainrow').length+' meldingen geplaatst door de NS</strong><br/></div></div>');
				var html = '<div class="modal fade" id="trainweb" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
				  html+='<div class="modal-dialog">';
					html+='<div class="modal-content">';
					  html+='<div class="modal-header">';
						html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
						html+='<h4 class="modal-title" id="myModalLabel2">NS Info</h4>';
					  html+='</div>';
					  html+='<div class="modal-body">';
						  html+='<iframe data-src="https://nustoring.nl/treinstoringen" width="100%" height="570" frameborder="0" allowtransparency="true"></iframe> '; 
					  html+='</div>';
					html+='</div>';
				  html+='</div>';
				html+='</div>';
				$('header').append(html);
			}
		},
		error: function(){ $('.containstrain').hide(); }
	});
	
}